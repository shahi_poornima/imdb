﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Imdb.Domain;

namespace Imdb.Repository
{
    public class ProducerRepository
    {
        private readonly List<Person> _person;
        public ProducerRepository()
        {
            _person = new List<Person>();
        }

        public void Add(Person person)
        {
            _person.Add(person);
        }

        public List<Person> Get()
        {
            return _person.ToList();
        }

    }
}
