﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imdb.Domain
{
    public class Movie
    {
        public string Name { get; set; }
        public int Year { get; set; }

        public string Plot { get; set; }
        public List<Person> Actors { get; set; }
        public Person Producer { get; set; }



        /*public Movie(string name, int year ,string plot,List<string> actors,string producers)
            
        {
            Name = name;
            Year = year;
            Plot = plot;
            Actors = actors;
            Producer = producers;
        }
        */
        public Movie()
        {
        }
    }
}
