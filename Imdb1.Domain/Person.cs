﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Imdb.Domain
{
    public class Person
    {
        public string Name { get; set; }
        public DateTime Dob { get; set; }

        public Person(string name, DateTime Dobid)
        {
            Name = name;
            Dob = Dobid;
        }

        public Person()
        {
        }


    }
}
