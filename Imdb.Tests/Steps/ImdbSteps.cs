﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Imdb.Domain;
using Imdb;
using NUnit.Framework;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace Imdb.Tests.Features
{
    [Binding]
    public class ImdbSteps
    {
        private ImdbService _imdbservice;
        private string _name, _plot;
        private Person _producer;
        private int _year;

        private List<Person> _actors;
        private List<Movie> _movies;
        public ImdbSteps()
        {
            _imdbservice = new ImdbService();
            _actors = new List<Person>();


        }
        [Given(@"the name of movie is ""(.*)""")]
        public void GivenTheNameOfMovieIs(string name)
        {
            _name = name;
        }

        [Given(@"the year of release is (.*)")]
        public void GivenTheYearOfReleaseIs(int year)
        {
            _year = year;
        }

        [Given(@"the plot of movie is ""(.*)""")]
        public void GivenThePlotOfMovieIs(string plot)
        {
            _plot = plot;
        }



        [Given(@"the actors are ""(.*)""")]
        public void GivenTheActorsAre(Decimal actor)
        {
            string str = actor.ToString();
            List<char> ch = str.ToCharArray().ToList();
            int[] actorIndexes = ch.Select(a => a - '0').ToArray();
            var actorList = _imdbservice.GetPerson(PersonType.actor);
            foreach (var actorIndex in actorIndexes)
            {
                _actors.Add(actorList[actorIndex - 1]);
            }
        }


        [Given(@"producer is (.*)")]
        public void GivenProducerIs(int p0)
        {
            _producer = _imdbservice.GetPerson(PersonType.producer)[p0 - 1];

        }

        [When(@"the user added movie")]
        public void WhenTheUserAddedMovie()
        {
            _imdbservice.AddMovie(_name, _year, _plot, _actors, _producer);
            _movies = _imdbservice.GetMovies();
        }

        [Then(@"the result will be like")]
        public void ThenTheResultWillBeLike(Table table)
        {
            
            
            table.CompareToSet(_movies.Select(c => new
            {
                name = c.Name,
                year = c.Year,
                plot = c.Plot,
                producer = c.Producer.Name

            })) ;


           

        }

        [Then(@"the actor list would be like")]
        public void ThenTheActorListWouldBeLike(Table table)
        {
            
            table.CompareToSet(_movies.Select(c => new
            {
                Moviename = c.Name,
                actors= String.Join(",", c.Actors.Select(x => x.Name.ToString())),

            }));
           

        }


        [Given(@"the list of movies")]
        public void GivenTheListOfMovies()
        {

        }

        [When(@"the user fetches movies")]
        public void WhenTheUserFetchesMovies()
        {
            _movies = _imdbservice.GetMovies();

        }

        [Then(@"the result should show like this")]
        public void ThenTheResultShouldShowLikeThis(Table table)
        {
            table.CompareToSet(_movies.Select(c => new
            {
                name = c.Name,
                year = c.Year,
                plot = c.Plot,
                producer = c.Producer.Name

            }));

        }




        [Then(@"the actors list is like")]
        public void ThenTheActorsListIsLike(Table table)
        {
            
            table.CompareToSet(_movies.Select(c => new
            {
                Moviename = c.Name,
                actors = String.Join(",", c.Actors.Select(x => x.Name.ToString())),

            }));


        }

        [BeforeScenario("addmovies")]
        public void AddSampleActorForAdd()
        {
            _imdbservice.AddPerson(PersonType.actor,"Actor1", new DateTime(2000, 03, 15));
            _imdbservice.AddPerson(PersonType.actor,"Actor2", new DateTime(2000, 03, 15));
            _imdbservice.AddPerson(PersonType.producer,"Producer1", new DateTime(2000, 03, 15));
        }

        [BeforeScenario("listmovies")]
        public void AddSampleBookForAdd()
        {
            _imdbservice.AddMovie("Movie1", 1998, "sample plot", new List<Person>(){
               new Person("Actor1", new DateTime(1988, 04, 30)),
                new Person("Actor2", new DateTime(1988, 04, 30))
            }, new Person("Producer1", new DateTime(1988, 04, 30)));
            _imdbservice.AddMovie("Movie2", 1998, "sample plot", new List<Person>(){
               new Person("Actor1", new DateTime(1988, 04, 30)),
                new Person("Actor2", new DateTime(1988, 04, 30))
            }, new Person("Producer1", new DateTime(1988, 04, 30)));


        }
    }
}