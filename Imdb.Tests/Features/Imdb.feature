﻿Feature: Imdb
    Simple imdb like project for adding and displaying movies



@addmovies
Scenario: Add Movies
    Given the name of movie is "Movie1"
    And the year of release is 2000
    And the plot of movie is "sample plot"
    And the actors are "1,2"
    And producer is 1
    When the user added movie
    Then the result will be like
    | Name   | Year | Plot        | Producer  |
    | Movie1 | 2000 | sample plot | Producer1 |
    And the actor list would be like
    | MovieName | Actors        |
    | Movie1    | Actor1,Actor2 |
@listmovies
 Scenario: List movie
    Given the list of movies
    When the user fetches movies
    Then the result should show like this
    | Name   | Year | Plot        | Producer  |
    | Movie1 | 1998 | sample plot | Producer1 |
    | Movie2 | 1998 | sample plot | Producer1 |
    And the actors list is like
    | MovieName | Actor         |
    | Movie1    | Actor1,Actor2 |
    | Movie2    | Actor1,Actor2 |