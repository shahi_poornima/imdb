﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using Imdb.Domain;
using Imdb.Repository;

namespace Imdb
{
    class Program
    {
        
        static void Main(string[] args)
        {            
            var imdbObject = new ImdbService();
            imdbObject.AddMovie("Movie1", 1998, "sample plot", new List<Person>(){
                new Person("Actor1", new DateTime(1998, 05, 20)),
                new Person("Actor2", new DateTime(1988, 04, 30))
            }, new Person("Producer1", new DateTime(1988, 04, 30)));
            imdbObject.AddPerson(PersonType.Actor,"Actor1", new DateTime(1998, 05, 20));
            imdbObject.AddPerson(PersonType.Actor, "Actor2", new DateTime(1988, 04, 30));
            imdbObject.AddPerson(PersonType.Producer, "Producer1", new DateTime(1988, 04, 30));

            Console.WriteLine("1.List Movies\n2.Add Movie\n3.Add Actor\n4.Add Producer\n5.Delete Movie\n6.Exit");
            int flag = 1;
            while (flag == 1)
            {
                Console.WriteLine("Enter your choice!!");
                var selectedOption = Convert.ToInt32(Console.ReadLine());
                switch (selectedOption)
                {
                    case 1:
                        foreach (var movie in imdbObject.GetMovies())
                        {
                            Console.WriteLine("Movie:" + " " + movie.Name);
                            Console.WriteLine("Year:" + " " + movie.Year);
                            Console.WriteLine("Plot" + " " + movie.Plot);
                            Console.Write("Actors:  ");
                            foreach (var actor in movie.Actors)
                            {
                                Console.Write(actor.Name + ' ');
                            }
                            Console.WriteLine();
                            Console.Write("Producer: " + " " + movie.Producer.Name);
                            Console.WriteLine();
                        }
                        break;
                    case 2:
                        Console.Write("Name:");
                        var movieName = Console.ReadLine();
                        Console.Write("Year:");
                        var movieYear = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Plot:");
                        var moviePlot = Console.ReadLine();
                        Console.WriteLine("Choose Actors");
                        int actorindex = 1;
                        List<Person> movieactors = new List<Person>();
                        var getActor = new List<Person>();
                        getActor = imdbObject.GetPerson(PersonType.Actor);
                        int ActorCount = getActor.Count;
                        foreach (var actor in getActor)
                        {
                            Console.WriteLine(actorindex + "." + actor.Name);
                            actorindex++;
                        }
                        var givenActors = Console.ReadLine().Split(' ');
                        foreach (var actorIndex in givenActors)
                        {
                            int ActorIndex = Convert.ToInt32(actorIndex);
                            if (ActorIndex>ActorCount)
                            {
                                throw new Exception("choose index with in the range");
                            }
                            movieactors.Add(getActor[ActorIndex - 1]);
                        }
                        Console.WriteLine("Choose Producer:");
                        int producerindex = 1;
                        var getProducer = new List<Person>();
                        getProducer = imdbObject.GetPerson(PersonType.Producer);
                        int ProducerCount = getProducer.Count;
                        foreach (var producer in getProducer)
                        {
                            Console.WriteLine(producerindex + "." + producer.Name);
                            producerindex++;
                        }
                        var givenProducer = Console.ReadLine();
                        int ProducerIndex = Convert.ToInt32(givenProducer);
                        if (ProducerIndex > ActorCount)
                        {
                            throw new Exception("choose index with in the range");
                        }
                        imdbObject.AddMovie(movieName, movieYear, moviePlot, movieactors, getProducer[ProducerIndex - 1]);
                        Console.WriteLine("Movie Added succesfully");
                        break;
                    case 3:
                        Console.Write("Name:");
                        var acName = Console.ReadLine();
                        Console.Write("DOB:");
                        try
                        {
                            var acDob = Convert.ToDateTime(Console.ReadLine());
                            imdbObject.AddPerson(PersonType.Actor, acName, acDob);
                        }
                        catch
                        {
                            throw new ArgumentException("Enter date correctly as yyyy/mm/dd");
                        }
                        
                        Console.WriteLine("Actor added succesfully");
                        break;
                    case 4:
                        Console.Write("Name:");
                        var producerName = Console.ReadLine();
                        Console.Write("DOB:");
                        try
                        {
                            var producerDob = Convert.ToDateTime(Console.ReadLine());
                            imdbObject.AddPerson(PersonType.Producer, producerName, producerDob);
                        }
                        catch
                        {
                            throw new ArgumentException("Enter date correctly as yyyy/mm/dd");
                        }
                        Console.WriteLine("Producer Added Successfully");
                        break;
                    case 5:
                        Console.WriteLine("Enter movie name");
                        var movieNameToDelete = Console.ReadLine();
                        if(string.IsNullOrWhiteSpace(movieNameToDelete))
                        {
                            throw new ArgumentException("Invalid arguments");
                        }
                        imdbObject.DeleteMovie(movieNameToDelete);
                        break;
                    case 6:
                        flag = 0;
                        break;
                    default:
                        Console.WriteLine("Enter correct option");
                        break;
                }
            }
        }
    }
}
