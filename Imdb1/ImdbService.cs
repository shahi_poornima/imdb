﻿using System;
using System.Collections.Generic;

using Imdb.Domain;
using Imdb.Repository;

namespace Imdb
{
    
    public class ImdbService
    {
        private readonly MovieRepository _movieRepository;
        private readonly ActorRepository _actorRepository;
        private readonly ProducerRepository _producerRepository;


        public ImdbService()
        {
            _movieRepository = new MovieRepository();
            _actorRepository = new ActorRepository();
            _producerRepository = new ProducerRepository();

        }

        public void AddMovie(string name, int year, string plot, List<Person> actors,Person producers)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Invalid arguments");
            }

            Movie movie = new Movie()
            {
                Name = name,
                Year = year,
                Plot = plot,
                Actors = actors,
                Producer = producers
            };

            _movieRepository.Add(movie);
        }
        public void AddPerson(PersonType movieConstant,string name, DateTime Dobid)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ArgumentException("Invalid arguments");
            }

            var actor = new Person()
            {
                Name = name,
                Dob = Dobid
            };
            if (movieConstant==PersonType.Actor)
            {
                // 1 ---Actor
                // 2 ---Producer
                _actorRepository.Add(actor);

            }
            else
            {
                _producerRepository.Add(actor);
            }
           

            
        }



        public void DeleteMovie(string name)
        {
            var list = _movieRepository.Get();
            foreach (var movie in list)
            {
                
                if (name.Equals(movie.Name))
                {
                    _movieRepository.Remove(movie);
                }
            }

        }
        public List<Person> GetPerson(PersonType movieConstant)
        {
            if (movieConstant == PersonType.Actor)
            {
                return _actorRepository.Get();
            }
            else 
            {
                return _producerRepository.Get();
            }
            
            
        }
        public List<Movie> GetMovies()
        {
            return _movieRepository.Get();
        }
    }
}
